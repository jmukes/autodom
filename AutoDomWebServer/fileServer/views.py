import os
import shutil
import datetime
from django.shortcuts import render

def randomHTMLFromTestCase(request):
    try:
        cpath = os.getcwd()
        testcasePath = os.path.dirname(cpath) + '/testcase/'
        outPath = os.path.dirname(cpath) + '/Tested/'
        file = os.listdir(testcasePath)[0]
        shutil.copyfile(testcasePath + file, cpath + "/fileServer/templates/index.html")
        shutil.move(testcasePath + file, outPath)
        os.rename(outPath + file, outPath + file + datetime.now())
    except IndexError:
        return render(request, 'done.html')
    except OSError:
        return render(request, "OOO.html")
    except Exception as error:
        print(error)
        context = {'error' : error}
        return render(request, 'error.html', context)
    return render(request, "index.html")