# AutoDom

This is an automated browser fuzzer. It was built off of Google's Domato fuzzer. AutoDom starts a web-server then serves the html that was mutated with Domato. It then injects the html with a <meta> tag so the page automatically refreshes. 


To use simply just run

'python3 AutoDom.py'

Follow the prompt then pick a browser and go to http://localhost:8000. AutoDom injects a meta tag to force a
refresh every 5 seconds. Because of the fact that the html gets mangled the tag wont always work. There is also a chime when the page changes. Refreshing the page manually should also do the trick. 


TODO:
	Make an actual webserver
	write try catch in script