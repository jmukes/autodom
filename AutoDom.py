import glob
import os
import http.server
import socketserver
import time
import datetime
import threading
import sys
import time

def getcwd():
    return os.path.basename(os.getcwd())


class myThread (threading.Thread):
   def __init__(self, threadID, name, counter):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.counter = counter
   def run(self):
      serve()

class myThread2 (threading.Thread):
   def __init__(self, threadID, name, counter):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.counter = counter
   def run(self):
      checker()

def createTestCase():
    if os.path.isdir(getcwd()[:getcwd().rfind('/') + 1] + "testcase") == False:
        path = getcwd()[:getcwd().rfind('/') + 1] + "testcase"
        try:
            os.mkdir(path)
        except:
            print("[-] Creation of the directory %s failed\n" % path)
        else:
            print("[+] Successfully created the directory %s \n" % path)

    else:
        if not len(os.listdir((getcwd()[:getcwd().rfind('/') + 1] + "testcase"))) == 0:
            print("***\aThere are files in the test case directory***\n***If they remain they wil be included in the test***\n")
            choice = input("Are you sure you would like to proceed? [y/n]\n")
            if not choice == 'y' or choice == 'Y':
                exit()

def genFiles(number):
    print("[+] Generating files\n")
    if os.path.isdir(getcwd()[:getcwd().rfind('/') + 1] + "domato") == False:
        try:
            os.system("git clone https://github.com/googleprojectzero/domato.git")
        except:
            print("[-] There was a problem cloning the Domato repo. \nEither clone it or check your connection and try again")
            
    domato = getcwd()[:getcwd().rfind('/') + 1] + "domato/generator.py "
    os.system("python3 " + domato + "--output_dir testcase --no_of_files " + number)

    print("[+] " + str(number) + " files created and placed in testcase directory")
    pass


def fileprep():
    print("[+] Preparing testcases\n")
    testcasedir = getcwd()[:getcwd().rfind('/') + 1] + "testcase/"
    files = glob.glob(testcasedir + "*.html")
    meta = '<html> <head> <meta http-equiv="refresh" content="5"> </head>'

    for file in files:
        os.system("sed -i -e '1d' " + file)
        os.system("sed -i -e '1d' " + file)
        os.system("sed -i -e '1d' " + file)

        with open(file, 'r+') as f:
            file_data = f.read()
            f.seek(0, 0)
            f.write(meta.rstrip('\r\n') + '\n' + file_data)

    if os.path.isdir(getcwd()[:getcwd().rfind('/') + 1] + "Tested") == False:
        path = getcwd()[:getcwd().rfind('/') + 1] + "Tested"
        try:
            os.mkdir(path)
        except:
            print("[-] Creation of the directory %s failed\n" % path)
        else:
            print("[+] Successfully created the directory %s \n" % path)

def serve():
    print("[+] Getting Ready to serve\n")
    os.system('python3 AutoDomWebServer/manage.py makemigrations')
    os.system('python3 AutoDomWebServer/manage.py migrate')
    os.system('python3 AutoDomWebServer/manage.py runserver 0:8080')

def checker():
    while(len(os.listdir((getcwd()[:getcwd().rfind('/') + 1] + "testcase"))) == 0):
        print("[+] All files have been used. Press CTR C to stop")
        time.sleep(60)
        

createTestCase()

ans = input("[+] How many files would you like to be created?\n\n>> ")
if ans.isdigit():
    genFiles(ans)
else:
    print("Not an integer, Exiting!")
    exit()
fileprep()
thread1 = myThread(1,"serve", 0)
thread2 = myThread2(2, "rename", 0)


thread1.start()
thread2.start()

